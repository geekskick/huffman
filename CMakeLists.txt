cmake_minimum_required(VERSION 3.5.0)
project(huffman VERSION 0.1.0 LANGUAGES C CXX)


add_library(huffman_core INTERFACE huffman.hpp heap.hpp)
target_compile_features(huffman_core INTERFACE cxx_std_17)
target_compile_options(huffman_core INTERFACE -Werror -Wfatal-errors -O0 -g -Wall -Wextra)

add_executable(catch_main test.cpp)
target_include_directories(catch_main PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
set(url https://github.com/philsquared/Catch/releases/download/v2.13.6/catch.hpp)
file(
  DOWNLOAD ${url} "${CMAKE_CURRENT_BINARY_DIR}/catch.hpp"
  STATUS status
  EXPECTED_HASH SHA256=681e7505a50887c9085539e5135794fc8f66d8e5de28eadf13a30978627b0f47)
list(GET status 0 error)
if(error)
  message(FATAL_ERROR "Could not download ${url}")
endif()
target_compile_options(catch_main PRIVATE -fsanitize=address,undefined)
target_link_options(catch_main PRIVATE -fsanitize=address,undefined)
target_include_directories(catch_main PUBLIC "${CMAKE_CURRENT_BINARY_DIR}")
target_link_libraries(catch_main PRIVATE huffman_core)

add_executable(huffman main.cpp)
set(url https://raw.githubusercontent.com/p-ranav/argparse/v2.9/include/argparse/argparse.hpp)
file(
  DOWNLOAD ${url} "${CMAKE_CURRENT_BINARY_DIR}/argparse.hpp"
  STATUS status
  EXPECTED_HASH SHA256=4ed054a8e9a454bf00b7261e1e758092373c5d77bdb4c648b0598a4cb9b0a619)
list(GET status 0 error)
if(error)
  message(FATAL_ERROR "Could not download ${url}")
endif()
target_include_directories(huffman PUBLIC "${CMAKE_CURRENT_BINARY_DIR}")
target_link_libraries(huffman PRIVATE huffman_core)

add_executable(huffman-san main.cpp)
target_compile_options(huffman-san PRIVATE -fsanitize=address,undefined)
target_link_options(huffman-san PRIVATE -fsanitize=address,undefined)
target_include_directories(huffman-san PUBLIC "${CMAKE_CURRENT_BINARY_DIR}")
target_link_libraries(huffman-san PRIVATE huffman_core)