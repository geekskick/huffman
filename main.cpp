#include "argparse.hpp"
#include "huffman.hpp"
#include <iterator>
#include <string_view>
int main(const int argc, const char **argv) {

    argparse::ArgumentParser program(argv[0]);
    program.add_argument("string").help("The string to huffman");
    program.add_argument("--progress")
        .help("Show progress as the trees are built up")
        .default_value(false)
        .implicit_value(true);
    program.add_argument("--tree").help("Only print the resulting tree").default_value(false).implicit_value(true);
    program.add_argument("--table").help("Show the huffman table").default_value(false).implicit_value(true);
    program.add_argument("--json")
        .help("Show the compressed output as a json")
        .default_value(false)
        .implicit_value(true);
    program.add_argument("--explain")
        .help("Show the binary output string but explain the structure")
        .default_value(false)
        .implicit_value(true);
    program.add_argument("--compress")
        .help("Run in compression mode. So the 'string' argument is the string to compress")
        .default_value(false)
        .implicit_value(true);

    try {
        program.parse_args(argc, argv);
    } catch (const std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }
    const auto progress = program["progress"] == true;
    const auto show_tree = program["tree"] == true;
    const auto show_table = program["table"] == true;
    const auto show_json = program["json"] == true;
    const auto explain = program["explain"] == true;
    //const auto compress = program["compress"] == true;

    auto viewer = [&]() { return progress ? std::make_unique<HuffmanViewer>() : nullptr; }();
    auto input = program.get<std::string>("string");
    auto word = std::string{input};
    std::replace(word.begin(), word.end(), ' ', '_');

    const auto tree = Huffman{word, std::move(viewer)};

    if (show_tree) {
        std::cout << tree << "\n";
        return EXIT_SUCCESS;
    }

    if (show_table) {
        std::cout << tree.table() << "\n";
        return EXIT_SUCCESS;
    }

    if (show_json) {
        std::cout << tree.to_json() << "\n";
        return EXIT_SUCCESS;
    }

    if (explain) {
        // This is currently bad BECAUSE it has a coupling between the huffman tree printing logic itself and the
        // text here, so if one changes so must the other (perhaps coupling isn't fair maybe duplication is more
        // correct) so I need to think of a better way of doing that
        std::cout << "Output Explanation:\n";
        std::cout << "The first " << sizeof(tree.packet().header.number_of_characters)
                  << " bytes are the number of letters in the message compressed\n\t";
        std::cout << tree.packet().header << "\n";

        std::cout << "The rest of the message is the compressed data, padded with 0s at the end.";
        std::cout << "For this message, that means there are " << tree.packet().bits.data.size() << "bytes of data.";
        std::cout << "This is represented as hex.\n\t";
        std::cout << tree.packet().bits << "\n";
        return EXIT_SUCCESS;
    }

    std::cout << tree.packet() << "\n";
}