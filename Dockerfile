FROM gcc:13.1.0 as builder

WORKDIR /cmake
RUN curl -OL https://github.com/Kitware/CMake/releases/download/v3.27.0/cmake-3.27.0-linux-x86_64.sh  && \
    sh cmake-3.27.0-linux-x86_64.sh --skip-license --prefix=/usr/local --exclude-subdir

RUN apt-get update && apt-get install -y --no-install-recommends \
	clang-format \
    cppcheck && \
    rm -rf /var/lib/apt/lists/*
