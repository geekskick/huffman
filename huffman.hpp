#pragma once
#include "heap.hpp"
#include <algorithm>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <set>
#include <sstream>
#include <string_view>
#include <unordered_map>
#include <vector>
struct Node {
    std::string name{};
    size_t count{};
    std::unique_ptr<Node> left{};
    std::unique_ptr<Node> right{};

    static std::unique_ptr<Node> New(std::string name, size_t count, std::unique_ptr<Node> left = nullptr,
                                     std::unique_ptr<Node> right = nullptr) {
        return std::unique_ptr<Node>(new Node{name, count, std::move(left), std::move(right)});
    }

    static std::unique_ptr<Node> Combine(std::unique_ptr<Node> l, std::unique_ptr<Node> r) {
        auto name = l->name + r->name;
        auto count = l->count + r->count;
        return Node::New(name, count, std::move(l), std::move(r));
    }

    bool is_leaf() const { return left == nullptr && right == nullptr; }

    bool operator<(const Node &other) const {
        if (counts_different(other)) {
            return count < other.count;
        }
        return name < other.name;
    }

    bool operator>(const Node &other) const {
        if (counts_different(other)) {
            return count > other.count;
        }
        return name > other.name;
    }

    std::string fullname() const { return name + std::to_string(count); }

    friend std::ostream &operator<<(std::ostream &os, const Node &n) {
        os << n.fullname() << ";\n";
        if (n.left) {
            os << n.fullname() << "->" << n.left->fullname() << ";\n";
            os << *n.left;
        }
        if (n.right) {
            os << n.fullname() << "->" << n.right->fullname() << ";\n";
            os << *n.right;
        }
        return os;
    }

private:
    bool counts_different(const Node &other) const { return count != other.count; }
};

void println(std::string_view rem, std::vector<std::unique_ptr<Node>> const &v) {
    std::cout << rem;
    for (const auto &e : v)
        std::cout << e->count << ' ';
    std::cout << '\n';
}

class LetterFrequencies {
public:
    using histogram_type = std::unordered_map<char, size_t>;
    explicit LetterFrequencies(std::string_view s) : _word{std::move(s)} {
        for (const auto letter : _word) {
            auto it = _histogram.find(letter);
            if (it == _histogram.cend()) {
                _histogram.emplace(letter, 1);
            } else {
                it->second++;
            }
        }
    };

    const histogram_type &histogram() const { return _histogram; }

    const std::string_view &word() const { return _word; }

private:
    std::string_view _word{};
    histogram_type _histogram{};
};

struct HuffmanViewer {
    size_t iteration{1};
    void print_tree(const std::vector<std::unique_ptr<Node>> &nodes) {
        std::ofstream f(std::to_string(iteration++) + ".dot");
        f << "digraph {";
        for (const auto &node : nodes) {
            f << *node;
        }
        f << "}";
    }
};

template <typename T> struct PtrGt {
    bool operator()(const std::unique_ptr<T> &l, const std::unique_ptr<T> &r) const { return *l > *r; }
};

class Serialiser {
public:
    explicit Serialiser(std::string_view message) : string_{message}, number_of_bits_{message.length()} {
        auto current_bit = 7;
        for (const auto &bit : message) {
            const auto binary = bit == '1' ? 1 : 0;
            if (current_bit == 7) {
                data_.push_back('\x00');
            }
            data_.back() |= binary << current_bit;
            current_bit = (current_bit - 1) < 0 ? 7 : current_bit - 1;
        }
    }

    size_t number_of_bits() const { return number_of_bits_; }
    const std::string &data() const { return data_; }

    friend std::ostream &operator<<(std::ostream &os, [[maybe_unused]] const Serialiser &s) {
        os << "Serialiser!";
        return os;
    }

private:
    std::string_view string_{};
    size_t number_of_bits_{};
    std::string data_{};
};

template <typename KeyType, typename ValueType> class HuffmanTable {
public:
    using TableType = std::unordered_map<KeyType, ValueType>;
    void insert(typename TableType::value_type pair) { data_.insert(std::move(pair)); }
    size_t size() const { return data_.size(); }
    const typename TableType::mapped_type &at(const typename TableType::key_type &key) const { return data_.at(key); }

    typename TableType::const_iterator begin() const { return data_.begin(); }
    typename TableType::const_iterator end() const { return data_.end(); }

    friend std::ostream &operator<<(std::ostream &os, const HuffmanTable &t) {
        for (const auto &pair : t) {
            os << pair.first << "\t" << pair.second << "\n";
        }
        return os;
    }

    const std::string &to_json() const {
        static std::string rc{};
        if (rc == std::string{}) {
            std::stringstream bruce;
            bruce << "[";
            for (size_t i = 0; i < data_.size(); ++i) {
                const auto &pair = std::next(data_.begin(), i);
                bruce << "{\"" << pair->first << "\":\"" << pair->second << "\"}";
                if (i != data_.size() - 1) {
                    bruce << ",";
                }
            }
            bruce << "]";
            rc = bruce.str();
        }
        return rc;
    }

private:
    TableType data_{};
};
class Huffman {
public:
    explicit Huffman(const std::string_view &message, std::unique_ptr<HuffmanViewer> viewer = nullptr)
        : viewer_{std::move(viewer)} {
        if (message == "") {
            return;
        }

        packet_.header.number_of_characters = message.length();
        root_ = _create_tree(message);
        _construct_node_code(root_, "", [&](const Node &node, const std::string &code) {
            table_.insert(std::make_pair(node.name[0], code));
        });
        // Horribly inefficient way of compressing the data because I create a huge
        // long string and then do a 'pack' type of operation to divide it's size by 8
        auto string_bin = std::string{};
        for (const auto &letter : message) {
            string_bin.append(table_.at(letter));
        }
        packet_.bits.data = Serialiser{string_bin}.data();
    }

    using TableType = HuffmanTable<char, std::string>;
    const TableType &table() const { return table_; }

    struct Header {
        size_t number_of_characters{};

        friend std::ostream &operator<<(std::ostream &os, const Header &h) {
            os << std::hex << std::setfill('0') << std::setw(sizeof(decltype(h.number_of_characters)))
               << h.number_of_characters;
            return os;
        }
    };
    struct Payload {
        std::string data{}; // follow the example of paho.mqtt.cpp where they use a string to store blocs of data

        friend std::ostream &operator<<(std::ostream &os, const Payload &p) {
            for (size_t i = 0; i < p.data.size(); i++) {
                const auto byte = static_cast<int>(p.data.at(i));
                os << std::hex << (byte & 0xff);
            }
            return os;
        }
    };
    struct Footer {
        friend std::ostream &operator<<(std::ostream &os, [[maybe_unused]] const Footer &f) { return os; }
    };
    struct Packet {
        Header header{};
        Payload bits{};
        Footer footer{};
        friend std::ostream &operator<<(std::ostream &os, const Packet &p) {
            os << p.header;
            os << p.bits;
            os << p.footer;
            return os;
        }
    };

    const std::string &to_json() const {
        static std::string rc{};
        if (rc == std::string{}) {
            std::stringstream bruce;
            bruce << "{\"huffman table\":" << table_.to_json();
            bruce << ",\"number of characters\":" << packet().header.number_of_characters << ",\"data\":\""
                  << packet().bits << "\"}";
            rc = bruce.str();
        }
        return rc;
    }

    const Packet &packet() const { return packet_; }

    friend std::ostream &operator<<(std::ostream &os, const Huffman &h) {
        os << "digraph {";
        os << *h.root_;
        os << "}";
        return os;
    }
    const std::unique_ptr<Node> &root() const { return root_; }

private:
    std::unique_ptr<Node> root_{};
    TableType table_{};
    Packet packet_{};
    std::unique_ptr<HuffmanViewer> viewer_{};

    std::unique_ptr<Node> _create_tree(const std::string_view &message) {

        const auto histogram = LetterFrequencies{message}.histogram();
        auto letters = _histogram_to_nodes(histogram);

        auto heap = MinPtrHeap<typename decltype(letters)::value_type>(letters);
        // println("Initial heap", letters);
        if (viewer_) {
            viewer_->print_tree(letters);
        }
        while (letters.size() > 1) {
            auto small_one = heap.pop();
            auto small_two = heap.pop();

            auto n = (Node::Combine(std::move(small_one), std::move(small_two)));
            heap.push(std::move(n));

            if (viewer_) {
                viewer_->print_tree(letters);
            }
        }

        auto rc = std::unique_ptr<Node>{};
        std::swap(rc, letters.front());
        return rc;
    }

    template <typename CodeCallback>
    void _construct_node_code(const std::unique_ptr<Node> &node, std::string code, const CodeCallback &cb) const {
        if (!node) {
            std::cerr << "The node was a nullptr which was unexpected\n";
            return;
        }
        if (node->is_leaf()) {
            cb(*node, code);
        }
        if (node->left) {
            _construct_node_code(node->left, code + "0", cb);
        }
        if (node->right) {
            _construct_node_code(node->right, code + "1", cb);
        }
    }

    std::vector<std::unique_ptr<Node>> _histogram_to_nodes(const LetterFrequencies::histogram_type &histogram) const {
        auto rc = std::vector<std::unique_ptr<Node>>{};
        std::transform(histogram.cbegin(), histogram.cend(), std::back_inserter(rc),
                       [](const auto &pair) { return Node::New(std::string{pair.first}, pair.second); });
        return rc;
    }
};
