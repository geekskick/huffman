#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "heap.hpp"
#include "huffman.hpp"
#include <climits>

TEST_CASE("Can use max", "heap") {
    auto data = std::vector<int>{3, 2, 4, 1, 5, 9};
    auto uut = MaxHeap<int>(data);

    SECTION("Creates heap") { REQUIRE(data == std::vector<int>{9, 5, 4, 1, 2, 3}); }
    SECTION("Pushes") {
        uut.push(6);
        REQUIRE(data == std::vector<int>{9, 5, 6, 1, 2, 3, 4});
    }
    SECTION("Pop") {
        auto popped = uut.pop();
        REQUIRE(data == std::vector<int>{5, 3, 4, 1, 2});
        REQUIRE(popped == 9);
    }
}

TEST_CASE("Can use min", "heap") {
    auto data = std::vector<int>{3, 2, 4, 1, 5, 9};
    auto uut = MinHeap<int>(data);
    SECTION("creates") { REQUIRE(data == std::vector<int>{1, 2, 4, 3, 5, 9}); }
    SECTION("pushes") {
        uut.push(6);
        REQUIRE(data == std::vector<int>{1, 2, 4, 3, 5, 9, 6});
    }
    SECTION("pops") {
        auto popped = uut.pop();
        REQUIRE(data == std::vector<int>{2, 3, 4, 9, 5});
        REQUIRE(popped == 1);
    }
    SECTION("pop leaves in order") {
        int previous_popped = INT_MIN;
        auto previous_size = data.size();
        while (!data.empty()) {
            const auto popped = uut.pop();
            REQUIRE(previous_popped <= popped);
            previous_popped = popped;

            REQUIRE(previous_size > data.size());
            previous_size = data.size();
        }
    }
}

std::vector<std::unique_ptr<int>> generate_ptr_vec(const std::vector<int> &values) {
    auto data = std::vector<std::unique_ptr<int>>{};
    std::transform(values.cbegin(), values.cend(), std::back_inserter(data),
                   [](const int v) { return std::make_unique<int>(v); });
    return data;
}

TEST_CASE("Can use ptrs", "heap") {
    SECTION("max heap") {
        auto data = generate_ptr_vec({3, 1, 4, 1, 5, 9});
        auto uut = MaxPtrHeap<decltype(data)::value_type>(data);
        auto popped = uut.pop();
        REQUIRE(*popped == 9);
        REQUIRE(data.size() == 5);
    }
    SECTION("Min heap") {
        auto data = generate_ptr_vec({3, 1, 4, 1, 5, 9});
        auto uut = MinPtrHeap<decltype(data)::value_type>(data);
        auto popped = uut.pop();
        REQUIRE(data.size() == 5);
        REQUIRE(*popped == 1);
    }
}

TEST_CASE("Can count letters", "histogram") {
    constexpr auto word = std::string_view{"abcde"};
    const auto uut = LetterFrequencies{word};
    const auto expected = LetterFrequencies::histogram_type{{'a', 1}, {'b', 1}, {'c', 1}, {'d', 1}, {'e', 1}};
    REQUIRE(uut.histogram() == expected);
}

TEST_CASE("Can construct from str", "histogram") {
    constexpr auto word = std::string_view{"abcde"};
    const auto uut = LetterFrequencies{word};
    const auto expected = LetterFrequencies::histogram_type{{'a', 1}, {'b', 1}, {'c', 1}, {'d', 1}, {'e', 1}};
    REQUIRE(uut.histogram() == expected);
}

TEST_CASE("Can count repeated letters", "histogram") {
    const auto uut = LetterFrequencies{"abcdee"};
    const auto expected = LetterFrequencies::histogram_type{{'a', 1}, {'b', 1}, {'c', 1}, {'d', 1}, {'e', 2}};
    REQUIRE(uut.histogram() == expected);
}

TEST_CASE("Can handle an empty string", "histogram") {
    constexpr auto word = std::string_view{""};
    const auto uut = LetterFrequencies{word};
    const auto expected = LetterFrequencies::histogram_type{};
    REQUIRE(uut.histogram() == expected);
}

TEST_CASE("Can construct", "huffman") { const auto uut = Huffman{"abcde"}; }

TEST_CASE("Can default construct", "node") {
    const auto uut = Node{};
    REQUIRE(uut.left == nullptr);
    REQUIRE(uut.right == nullptr);
    REQUIRE(uut.name == std::string{});
    REQUIRE(uut.count == 0);
}

TEST_CASE("Can get fullname", "node") {
    const auto uut = Node{"a", 1};
    REQUIRE(uut.fullname() == "a1");
}

TEST_CASE("Can compare", "node") {
    const auto a = Node{"a", 1};
    const auto b = Node{"b", 2};
    SECTION("<") { REQUIRE(a < b); }
    SECTION(">") { REQUIRE(b > a); }
}

TEST_CASE("Can ptr construct", "node") {
    const auto uut = Node::New("a", 1);
    REQUIRE(uut->count == 1);
    REQUIRE(uut->name == "a");
    REQUIRE(uut->left == nullptr);
    REQUIRE(uut->right == nullptr);
}

TEST_CASE("Can combine", "node") {
    auto a = Node::New("a", 1);
    auto b = Node::New("b", 2);
    const auto uut = Node::Combine(std::move(a), std::move(b));
    REQUIRE(uut->name == "ab");
    REQUIRE(uut->count == 3);
    REQUIRE(uut->left->fullname() == "a1");
    REQUIRE(uut->right->fullname() == "b2");
}

TEST_CASE("Can print as dot", "node") {
    auto a = Node{"a", 1};
    std::stringstream ss;
    using Catch::Matchers::Contains;

    SECTION("leaf node") {
        ss << a;
        const auto uut = ss.str();
        REQUIRE_THAT(uut, Contains("a1;"));
    }
    SECTION("left child") {
        auto l = Node::New("left", 2);
        a.left = std::move(l);
        ss << a;
        const auto uut = ss.str();
        REQUIRE_THAT(uut, Contains("a1->left2;"));
    }
    SECTION("right child") {
        auto r = Node::New("right", 2);
        a.right = std::move(r);
        ss << a;
        const auto uut = ss.str();
        REQUIRE_THAT(uut, Contains("a1->right2;"));
    }
    SECTION("right and left child") {
        auto l = Node::New("left", 2);
        a.left = std::move(l);
        auto r = Node::New("right", 2);
        a.right = std::move(r);
        ss << a;
        const auto uut = ss.str();
        REQUIRE_THAT(uut, Contains("a1->right2;"));
        REQUIRE_THAT(uut, Contains("a1->left2;"));
    }
}

TEST_CASE("Can generate tree with 1 letter", "huffman") {

    const auto uut = Huffman{"a"};
    const auto &root = uut.root();
    REQUIRE(root->fullname() == "a1");
    REQUIRE(root->left == nullptr);
    REQUIRE(root->right == nullptr);
}

TEST_CASE("Can generate tree with 2 letter", "huffman") {

    const auto uut = Huffman{"ab"};
    const auto &root = uut.root();
    REQUIRE(root->fullname() == "ab2");
    // I dont care about the ordering of the nodes in the leaves
    REQUIRE((root->left->fullname() == "a1" || root->left->fullname() == "b1"));
    REQUIRE((root->right->fullname() == "a1" || root->right->fullname() == "b1"));
}

TEST_CASE("Can generate tree with 3 letter", "huffman") {

    const auto uut = Huffman{"abb"};
    const auto &root = uut.root();
    REQUIRE(root->fullname() == "ab3");
    // the smallest weighted letter should have been selected first
    REQUIRE(root->left->fullname() == "a1");
    REQUIRE(root->right->fullname() == "b2");
}

TEST_CASE("Can generate tree with loads of letters - root is correct", "huffman") {

    const auto uut = Huffman{"abbcccddddeeeee"};
    const auto &root = uut.root();
    REQUIRE(root->fullname() == "abcde15");
}

TEST_CASE("Can create table", "huffman") {
    /* The tree should be constructed as follows:
        a1 b2 c3 d4 e5
        ab3 c3 d4 e5
        d4 e5 abc6
        abc6 de9
        abcde15

        Resulting in
                 root
                /   \
            abc      de
            / \      /\
           ab  c    d  e
           /\
          a  b
        Because there are 5 letters we know we need 3 bits to represent them
    */
    const auto uut = Huffman{"abbcccddddeeeee"};
    const auto table = uut.table();
    REQUIRE(table.at('e') == "11");
    REQUIRE(table.at('d') == "10");
    REQUIRE(table.at('c') == "01");
    REQUIRE(table.at('a') == "000");
    REQUIRE(table.at('b') == "001");
}
TEST_CASE("Takes into account alphabet with ordering", "node") {
    const auto a = Node{"a", 1};
    const auto b = Node{"b", 1};
    REQUIRE(a < b);
    REQUIRE(b > a);
}

TEST_CASE("Doesn't generate tree with 0 letters", "huffman") {

    const auto uut = Huffman{""};
    const auto &root = uut.root();
    REQUIRE(root == nullptr);
}

TEST_CASE("Packet has correct header", "huffman") {
    SECTION("with something in the string") {
        const auto uut = Huffman{"a"};
        REQUIRE(uut.packet().header.number_of_characters == 1);
    }
    SECTION("With nothin in string") {
        const auto uut = Huffman{""};
        REQUIRE(uut.packet().header.number_of_characters == 0);
    }
}

TEST_CASE("Packet has correct data", "huffman") {
    const auto uut = Huffman{"abbcccddddeeeee"};
    const auto &packet = uut.packet();
    const auto &data = packet.bits.data;

    const auto table = uut.table();
    CHECK(table.at('e') == "11");
    CHECK(table.at('d') == "10");
    CHECK(table.at('c') == "01");
    CHECK(table.at('a') == "000");
    CHECK(table.at('b') == "001");
    // 000 001 001 01 01 01 10 10 10 10 11 11 11 11 11
    // grouped by nibbles:
    // 0000 0100 1010 1011 0101 0101 1111 1111 1(000)
    //   0    4    a   b    5    5     f   f      8
    constexpr auto expected = std::string_view{"\x04\xab\x55\xff\x80"};
    REQUIRE(expected == data);
}

TEST_CASE("String to binary with 0", "serialiser") {
    const auto uut = Serialiser{"0"};
    REQUIRE(uut.number_of_bits() == 1);
    // We have plonked a 0 filled byte (string term) in there
    // so string comparison wont work

    // as a proof we check an empty string size is less
    CHECK(std::string{}.size() == 0);
    REQUIRE(uut.data().size() == 1);
    // REQUIRE(uut.data() == "\x00");
}

TEST_CASE("String to binary with 9 bits", "serialiser") {
    const auto uut = Serialiser{"010101011"};
    REQUIRE(uut.number_of_bits() == 9);
    // We should go into a second byte here
    REQUIRE(uut.data().size() == 2);
    REQUIRE(uut.data() == "\x55\x80");
}

TEST_CASE("String to binary with 0 bits", "serialiser") {
    const auto uut = Serialiser{""};
    REQUIRE(uut.number_of_bits() == 0);
    REQUIRE(uut.data().size() == 0);
}

TEST_CASE("Can jsonify", "huffman") {
    using Catch::Matchers::Matches;

    const auto uut = Huffman{"hello"};
    const auto &json = uut.to_json();
    REQUIRE_THAT(json, Matches(".*\"data\"\\s*:\\s*\"f880\".*"));
    REQUIRE_THAT(json,
                 Matches(".*\"huffman "
                         "table\"\\s*:\\s*\\[\\s*\\{\\s*\"h\"\\s*:\\s*\"111\"\\s*\\}\\s*,\\s*\\{\\s*\"e\"\\s*:\\s*"
                         "\"110\"\\s*\\}\\s*,\\s*\\{\\s*\"o\"\\s*:\\s*\"10\"\\s*\\}\\s*"
                         "\\s*,\\s*\\{\\s*\"l\"\\s*:\\s*\"0\"\\s*\\}\\s*\\].*"));
    REQUIRE_THAT(json, Matches(".*\"number of characters\"\\s*:\\s*5.*"));
}