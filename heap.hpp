#pragma once
#include <algorithm>
#include <deque>
#include <fstream>
#include <iostream>
#include <memory>
#include <set>
#include <sstream>
#include <string_view>
#include <unordered_map>
#include <vector>

template <typename T, typename Cmp> class Heap {
public:
    explicit Heap(std::vector<T> &underlying) : _underlying(underlying) {
        std::make_heap(_underlying.begin(), _underlying.end(), Cmp());
    }

    void push(T thing) {
        _underlying.push_back(std::move(thing));
        std::push_heap(_underlying.begin(), _underlying.end(), Cmp());
    }

    T pop() {
        T rc{};
        std::pop_heap(_underlying.begin(), _underlying.end(), Cmp());
        std::swap(rc, _underlying.back());
        _underlying.pop_back();
        return rc;
    }

private:
    std::vector<T> &_underlying{};
};

template <typename T> using MaxHeap = Heap<T, std::less<>>;
template <typename T> using MinHeap = Heap<T, std::greater<>>;

template <typename T, typename Cmp> struct PtrCompare {
    constexpr bool operator()(const T &lhs, const T &rhs) const { return Cmp()(*lhs, *rhs); }
};

template <typename T> using MaxPtrHeap = Heap<T, PtrCompare<T, std::less<>>>;
template <typename T> using MinPtrHeap = Heap<T, PtrCompare<T, std::greater<>>>;
